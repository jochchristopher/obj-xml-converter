# js-xml-convert

---

## What is this module about?

---

The Goal of the module was to provide an easy-to-use API for converting JS/TS Objects to XML and vice versa.

## Features

---

* Creating XML Schemas (XSD)

* Validating XML with Schema

* There are two API to choose from:
  * Promises (Ofc with clear error messages)
  * Regular Methods

So in case you want to chain the method in a Promise Chain, then the Promise API is the right choice. Otherwise just simply use the "normal" API.

## Installation

---

...

## API

---

### Methods

* obj2xml
  * promise.obj2xml
* xml2obj
  * promise.xml2obj
* validateXML

### Schema

* XSD

#### NOTE: XSD is a class!


## Examples

---

...

## Docs

---

```typescript
    
```