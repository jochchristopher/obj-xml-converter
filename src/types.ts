/**
 * @enum Enum that contains possible versions with description
 */
enum XMLVersions {
    /**
     * XML 1.0 was developed in 1996 by a working group of worldwide web consortium with several design goals.
     */
    one = "1.0",
    /**
     * The features of XML 1.1 are similar to XML 1.0 except for a few of the changes.
     */
    two = "1.1"
}
/**
 * @enum Enum that contains possible encodings
 *
 * @description Please let me know if your desired encoding is missing!
 */
enum XMLEncoding {
    UTF8 = "UTF-8",
    UTF16 = "UTF-16",
    UTF32 = "UTF-32",
    ASCII = "ASCII",
    EBCDIC = "EBCDIC",
    ISO_8859_1 = "ISO-8859-1",
    ISO_8859_2 = "ISO-8859-2",
    ISO_8859_3 = "ISO-8859-3",
    ISO_8859_4 = "ISO-8859-4",
    ISO_8859_5 = "ISO-8859-5",
    ISO_8859_6 = "ISO-8859-6",
    ISO_8859_7 = "ISO-8859-7",
    ISO_8859_8 = "ISO-8859-8",
    ISO_8859_9 = "ISO-8859-9",
    ISO_8859_10 = "ISO-8859-10",
    ISO_8859_11 = "ISO-8859-11",
    ISO_8859_13 = "ISO-8859-13",
    ISO_8859_14 = "ISO-8859-14",
    ISO_8859_15 = "ISO-8859-15",
    ISO_8859_16 = "ISO-8859-16",
    CP437 = "CP437",
    CP720 = "CP720",
    CP737 = "CP737",
    CP850 = "CP850",
    CP852 = "CP852",
    CP855 = "CP855",
    CP857 = "CP857",
    CP858 = "CP858",
    CP860 = "CP860",
    CP861 = "CP861",
    CP862 = "CP862",
    CP863 = "CP863",
    CP865 = "CP865",
    CP866 = "CP866",
    CP869 = "CP869",
    CP872 = "CP872",
    Windows_1250 = "Windows-1250",
    Windows_1251 = "Windows-1251",
    Windows_1252 = "Windows-1252",
    Windows_1253 = "Windows-1253",
    Windows_1254 = "Windows-1254",
    Windows_1255 = "Windows-1255",
    Windows_1256 = "Windows-1256",
    Windows_1257 = "Windows-1257",
    Windows_1258 = "Windows-1258",
    KOI8_R = "KOI8-R",
    KOI8_U = "KOI8-U",
    KOI7 = "KOI7",
    MIK = "MIK",
    ISCII = "ISCII",
    TSCII = "TSCII",
    VISCII = "VISCII",
    JISX0208 = "JIS X 0208",
    ShiftJIS = "Shift JIS",
    EUC_JP = "EUC-JP",
    ISO_2022_JP = "ISO-2022-JP",
    JISX0213 = "JIS X 0213",
    Shift_JIS_2004 = "Shift_JIS-2004",
    EUC_JIS_2004 = "EUC-JIS-2004",
    ISO_2022_JP_2004 = "ISO-2022-JP-2004",
    GB_2312 = "GB 2312",
    GBK = "GBK",
    GB_18030 = "GB 18030",
    Big5 = "Big5",
    HKSCS = "HKSCS",
    KS_X_1001 = "KS X 1001",
    EUC_KR = "EUC-KR",
    ISO_2022_KR = "ISO-2022-KR",
    ANSEL = "ANSEL"
}

/**
 * @interface Prolog
 * 
 * @description describes the structure of the prolog property
 */
interface Prolog {
    /**
     * @property defines the version of the xml document
    */
    version: XMLVersions;
    /**
     * @property defines the encoding of the xml document
     */
    encoding: XMLEncoding;
    /**
     * @property To be added
     */
    standalone?: 'yes' | 'no';
}

/**
 * @interface AttributeInterface
 * 
 * @description defines valid types for adding attributes to xml tag
 */
interface AttributeInterface {
    /**
     * @property
     */
    [attribute: string]: string;
}

/**
 * @interface Attribute
 * 
 * @description defines the structure of defining attributes for the xml tag
 */
interface Attribute {
    /**
    * ---
    * Used for defining attributes
    * ```typescript
    *  {
        [attribute: string]: string
       }
    * ```
    */
    "@"?: AttributeInterface;
}

/**
 * @description defines valid types for adding content to xml tag
 */
type ContentType = string;

/**
 * @interface Content
 * 
 * @description 
 */
interface Content {
    /**
     * @property used for defining the content of the xml element
     */
    "#": ContentType;
}
/**
 * @interface Root
 * 
 * @description describes the structure of the root element property
 */
interface Root extends Attribute {
    /**
     * @property used for defining the name of the root element
     */
    name: string;
}

/**
 * @interface XMLElementObject
 * 
 * @extends {Attribute} Interface that defines the structure of adding a attribute to the xml tag
 * @extends {Content} Interface that defines the structure of adding content to the xml tag
 * 
 * @description describes the structure of a xml object
 * 
 */
interface XMLElementObject extends Attribute, Content {

    [element: string]: ContentType | AttributeInterface | XMLElementObject | XMLElementObject[] | undefined;
}

/**
 * @interface ObjectSchema
 * 
 * @description used for providing validation and assistance for defining object
 */
interface ObjectSchema {
    /**
     * @property used for defining the prolog
     */
    prolog: Prolog;
    /**
     * @property used for defining the root element
     */
    root: Root;

    [element: string]: ContentType | Prolog | Root | XMLElementObject | XMLElementObject[];
}

const obj: ObjectSchema = {
    prolog: {
        encoding: XMLEncoding.UTF8,
        version: XMLVersions.one
    },
    root: {
        name: 'root'
    },
    test: {
        "#": '',
        test1: [
            {
                "#": '1',
            },
            {
                "#": '2'
            }
        ]
    }
}
export { ObjectSchema };